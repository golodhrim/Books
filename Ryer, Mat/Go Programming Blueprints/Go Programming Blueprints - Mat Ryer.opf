<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">179</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">6990dd8c-b736-47f3-9595-50d5e62d51f0</dc:identifier>
        <dc:title>Go Programming Blueprints</dc:title>
        <dc:creator opf:file-as="Ryer, Mat" opf:role="aut">Mat Ryer</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (5.26.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2016-10-27T19:34:48+00:00</dc:date>
        <dc:description>Build real-world, production-ready solutions in Go using cutting-edge technology and techniquesAbout This BookGet up to date with Go and write code capable of delivering massive world-class scale performance and availabilityLearn to apply the nuances of the Go language, and get to know the open source community that surrounds it to implement a wide range of start-up quality projectsWrite interesting and clever but simple code, and learn skills and techniques that are directly transferrable to your own projectsWho This Book Is ForIf you are familiar with Go and are want to put your knowledge to work, then this is the book for you. Go programming knowledge is a must.What You Will LearnBuild quirky and fun projects from scratch while exploring patterns, practices, and techniques, as well as a range of different technologiesCreate websites and data services capable of massive scale using Go's net/http package, exploring RESTful patterns as well as low-latency WebSocket APIsInteract with a variety of remote web services to consume capabilities ranging from authentication and authorization to a fully functioning thesaurusDevelop high-quality command-line tools that utilize the powerful shell capabilities and perform well using Go's in-built concurrency mechanismsBuild microservices for larger organizations using the Go Kit libraryImplement a modern document database as well as high-throughput messaging queue technology to put together an architecture that is truly ready to scaleWrite concurrent programs and gracefully manage the execution of them and communication by smartly using channelsGet a feel for app deployment using Docker and Google App EngineIn DetailGo is the language of the Internet age, and the latest version of Go comes with major architectural changes. Implementation of the language, runtime, and libraries has changed significantly. The compiler and runtime are now written entirely in Go. The garbage collector is now concurrent and provides dramatically lower pause times by running in parallel with other Go routines when possible.This book will show you how to leverage all the latest features and much more. This book shows you how to build powerful systems and drops you into real-world situations. You will learn to develop high-quality command-line tools that utilize the powerful shell capabilities and perform well using Go's in-built concurrency mechanisms. Scale, performance, and high availability lie at the heart of our projects, and the lessons learned throughout this book will arm you with everything you need to build world-class solutions. You will get a feel for app deployment using Docker and Google App Engine. Each project could form the basis of a start-up, which means they are directly applicable to modern software markets.Style and approachThis book provides fun projects that involve building applications from scratch. These projects will teach you to build chat applications, a distributed system, and a recommendation system.</dc:description>
        <dc:publisher>Packt Publishing Ltd</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9781786462473</dc:identifier>
        <dc:identifier opf:scheme="GOOGLE">15XcDgAAQBAJ</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>Computers</dc:subject>
        <dc:subject>Programming Languages</dc:subject>
        <dc:subject>General</dc:subject>
        <dc:subject>Web</dc:subject>
        <dc:subject>Programming</dc:subject>
        <dc:subject>Algorithms</dc:subject>
        <meta name="calibre:author_link_map" content="{&quot;Mat Ryer&quot;: &quot;&quot;}"/>
        <meta name="calibre:timestamp" content="2019-12-01T19:16:24+00:00"/>
        <meta name="calibre:title_sort" content="Go Programming Blueprints"/>
    </metadata>
    <guide>
        <reference type="cover" title="Cover" href="Go Programming Blueprints - Mat Ryer.jpg"/>
    </guide>
</package>
